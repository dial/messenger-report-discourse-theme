
Discourse theme for Internet Messenger Report forums.

Requires:
- [https://gitlab.com/dial/dial-discourse-theme](https://gitlab.com/dial/dial-discourse-theme)
- [https://github.com/mozilla/discourse-mozilla-theme](https://github.com/mozilla/discourse-mozilla-theme)
